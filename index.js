const inputCalc = document.querySelector('.account');
const resultCalc = document.querySelector('.result');

function input(item){
    inputCalc.value = inputCalc.value + item;
}

function result() {
    if( eval(inputCalc.value) === undefined){
        resultCalc.value = '0';
        inputCalc.value = '0';
    } else if (eval(inputCalc.value) === Infinity) {
        resultCalc.value = '0';
        inputCalc.value = '0';
    }
    resultCalc.value = eval(inputCalc.value);
    inputCalc.value = eval(inputCalc.value);
}

function backspace(){
    inputCalc.value = inputCalc.value.substr(0, inputCalc.value.length-1)
}

function clearAll() {
    inputCalc.value = '';
    resultCalc.value = '0';
}